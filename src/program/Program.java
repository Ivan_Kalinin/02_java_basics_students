package program;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import enums.Curriculum;

public class Program {
  private enum PrintedStudents {
    ALL, SURVIVING, NOT_SURVIVING;
  }

  private static int chanceMark; // probabilistic remaining mark to be survived
  private static double treshold; // threshold mark to be survived
  private static PrintedStudents who; // state of printed student from list

  public static void main(String[] args) {
    chanceMark = 5;
    treshold = 4.5;
    who = PrintedStudents.SURVIVING;

    // initialization of courses
    ArrayList<Lesson> javaEElessons = new ArrayList<Lesson>();
    javaEElessons.add(new Lesson("���������� Java Servlets", 16));
    javaEElessons.add(new Lesson("Struts Framework", 24));
    javaEElessons.add(new Lesson("Spring Framework", 48));
    javaEElessons.add(new Lesson("Hibernate", 20));
    Course javaEE = new Course(Curriculum.J2EE, javaEElessons, LocalDate.of(2012, 4, 1));

    ArrayList<Lesson> javalLessons = new ArrayList<Lesson>();
    javalLessons.add(new Lesson("����� ���������� Java", 8));
    javalLessons.add(new Lesson("���������� JFC/Swing", 16));
    javalLessons.add(new Lesson("���������� JDBC", 16));
    javalLessons.add(new Lesson("���������� JAX", 52));
    javalLessons.add(new Lesson("���������� commons", 44));
    Course java = new Course(Curriculum.JAVA, javalLessons, LocalDate.of(2012, 5, 1));

    // initialization of students
    ArrayList<Student> students = new ArrayList<Student>();
    students.add(new Student("Anna", "Shmeleva", java,
        new ArrayList<Integer>(Arrays.asList(3, 4, 5, 4, 3, 4, 5))));
    students.add(new Student("Ivan", "Ivanov", javaEE,
        new ArrayList<Integer>(Arrays.asList(4, 5, 4, 5, 4, 5, 3))));

    students.add(new Student("Petr", "Petrov", java,
        new ArrayList<Integer>(Arrays.asList(4, 5, 3, 2, 3, 3, 2, 5, 5, 5))));

    System.out.println("1) ������ ���������:");
    printStudentsList(students, PrintedStudents.ALL);

    // list sorting by avg point and printing
    Collections.sort(students, (student1, student2) 
        -> Double.compare(student1.getAveragePoint(), student2.getAveragePoint()));


    System.out.println("2) ��������������� �� �������� ����� ������:");
    printStudentsList(students, PrintedStudents.ALL);

    // list sorting by remaining hours and printing
    Collections.sort(students, (student1, student2) 
        -> Double.compare(student1.getRemainingHours(), student2.getRemainingHours()));

    System.out.println("3) ��������������� �� ���������� ����� ������:");
    printStudentsList(students, PrintedStudents.ALL);

    // demonstration filter system for student list by surviving and not criterion
    System.out.println(new StringBuilder().append("3) ��������������� �� ���������� ����� ������ ")
        .append(who).append(" ���������:"));
    printStudentsList(students, who);

    who = PrintedStudents.NOT_SURVIVING;
    System.out.println(new StringBuilder().append("3) ��������������� �� ���������� ����� ������ ")
        .append(who).append(" ���������:"));
    printStudentsList(students, who);
  }

  private static void printStudentsList(ArrayList<Student> students, PrintedStudents who) {
    String verdict = "";
    StringBuilder outString = new StringBuilder();

    for (Student student : students) {
      if (student.hasChanceNow(chanceMark, treshold)) {
        if (who.equals(PrintedStudents.NOT_SURVIVING))
          continue;
        verdict = "����� ���������� ��������.";
      } else {
        if (who.equals(PrintedStudents.SURVIVING))
          continue;
        verdict = "���������.";
      }
      outString.append(student.getFirstName()).append(" ").append(student.getSecondName())
          .append(" - �� ��������� �������� �� ��������� ").append(student.getCourse().getTitle())
          .append(" Developer �������� ").append(student.getRemainingDays()).append(" ��. (")
          .append(student.getRemainingHours()).append(" �.) ������� ���� ")
          .append(String.format("%.2f", student.getAveragePoint()))
          .append(". ��� ���������� ������� ").append(chanceMark).append(" ����� ����� ���� ")
          .append(String.format("%.2f", student.getChanceAvg(chanceMark))).append(". ")
          .append(verdict).append("\n");
    }

    System.out.println(outString);
  }
}
