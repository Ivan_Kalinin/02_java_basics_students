package program;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class Student {
  public Student(String firstName, String secondName, Course course, ArrayList<Integer> marks) {
    this.firstName = firstName;
    this.secondName = secondName;
    this.course = course;
    this.marks = marks;
  }

  private String firstName;
  private String secondName;
  private Course course;
  private ArrayList<Integer> marks;

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getSecondName() {
    return secondName;
  }

  public void setSecondName(String secondName) {
    this.secondName = secondName;
  }

  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public ArrayList<Integer> getMarks() {
    return marks;
  }

  public void setMarks(ArrayList<Integer> marks) {
    this.marks = marks;
  }

  public double getAveragePoint() {
    int sum = 0;
    for (int mark : marks)
      sum += mark;

    return (double) sum / marks.size();
  }

  public long getRemainingHours() {
    long result = this.getRemainingDays() * 8;
    return result;
  }

  public long getRemainingDays() {
    Period period = Period.ofDays(marks.size());
    LocalDate currentDate = course.getStartDate().plus(period);
    long remainingDays = ChronoUnit.DAYS.between(currentDate, course.getEndDate());

    return remainingDays;
  }

  public boolean hasChanceNow(int mark, double treshold) {
    if (getChanceAvg(mark) < treshold)
      return false;
    else
      return true;
  }

  public double getChanceAvg(int mark) {
    long remainingDays = this.getRemainingDays();
    int sum = 0;

    for (int m : marks)
      sum += m;
    for (int i = 0; i < remainingDays; i++)
      sum += mark;

    int allDays = this.course.getTotalDuration() / 8;
    return (double) sum / allDays;
  }
}
