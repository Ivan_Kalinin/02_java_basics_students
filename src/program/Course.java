package program;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;

import enums.Curriculum;

public class Course {
  private void init() {     
    for (Lesson lesson : lessonList) {
      this.totalDuration += lesson.getDuration();
    }

    Period period = Period.ofDays(totalDuration / 8);
    this.endDate = this.startDate.plus(period);
  }

  public Course(Curriculum title, ArrayList<Lesson> lessonList, LocalDate startDate) {
    this.title = title;
    this.lessonList = lessonList;
    this.startDate = startDate;
    init();
  }

  private Curriculum title;
  private LocalDate startDate;
  private LocalDate endDate;

  private ArrayList<Lesson> lessonList;
  private int totalDuration;

  public Curriculum getTitle() {
    return title;
  }

  public void setTitle(Curriculum title) {
    this.title = title;
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  public void setStartDate(LocalDate startDate) {
    this.startDate = startDate;
  }

  public ArrayList<Lesson> getLessonList() {
    return lessonList;
  }

  public void setLessonList(ArrayList<Lesson> lessonList) {
    this.lessonList = lessonList;
  }

  public int getTotalDuration() {
    return totalDuration;
  }

  public LocalDate getEndDate() {
    return endDate;
  }
}
