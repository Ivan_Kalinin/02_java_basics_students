package program;

public class Lesson {
  public Lesson(String title, int duration) {
    this.duration = duration;
    this.title = title;
  }

  private int duration;
  private String title;

  public int getDuration() {
    return duration;
  }

  public void setDuration(int duration) {
    this.duration = duration;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }
}
